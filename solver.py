import math


class Solver:
    def solve(self, a: float, b: float, c: float) -> [...]:
        self.check_special_float(a, b, c)
        self.check_zero_a(a)

        return self.calc(a, b, c)

    def calc(self, a: float, b: float, c: float) -> [...]:
        d = self.discriminant(a, b, c)

        if self.is_negative(d):
            result = []

        elif not self.is_zero(d):
            x1 = - (b + self.sign(b) * math.sqrt(d)) / (2 * a)
            x2 = c / (a * x1)

            result = sorted([x1, x2])

        else:
            result = [- b / (2 * a)]

        return result

    @staticmethod
    def discriminant(a: float, b: float, c: float) -> float:
        return b * b - 4 * a * c

    @staticmethod
    def is_negative(coeff: float) -> bool:
        return True if coeff < 0 else False

    @staticmethod
    def is_zero(coeff: float) -> bool:
        epsilon = 1e-10
        return True if coeff < epsilon else False

    def sign(self, coeff: float) -> int:
        return -1 if self.is_negative(coeff) else 1

    def check_zero_a(self, a: float) -> None:
        if self.is_zero(a):
            raise ZeroDivisionError('a cannot be a zero!')

    @staticmethod
    def check_special_float(a: float, b: float, c: float) -> None:
        if [coeff for coeff in [a, b, c] if coeff in [float('nan'), float('inf'), - float('inf')]]:
            raise ValueError('coefficient cannot be nan or inf!')
