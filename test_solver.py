import solver
import pytest


def test_no_roots():
    a = 1.
    b = 0.
    c = 1.

    assert solver.Solver().solve(a, b, c) == []


def test_two_roots():
    a = 1.
    b = 0.
    c = -1.

    assert solver.Solver().solve(a, b, c) == [-1., 1.]


def test_one_root():
    a = 1.
    b = 2.
    c = -1.25e-13 + 1.

    assert solver.Solver().solve(a, b, c) == [-1.]


def test_a_not_zero():
    a = 0.
    b = 1.
    c = 1.

    with pytest.raises(ZeroDivisionError):
        solver.Solver().solve(a, b, c)


def test_non_numeric_coeff():
    a = float('inf')
    b = float('nan')
    c = - float('inf')

    with pytest.raises(ValueError):
        solver.Solver().solve(a, b, c)
